/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_JSONDOCUMENT_HPP
#define OCL_GUARD_JSONDOCUMENT_HPP

#include "JsonValue.hpp"
#include <map>
#include <string>
#include <cstdio>

namespace ocl
{

// JSON value class, which supports all the types and conversions between them.
template<typename StringType = std::string,
    typename IntegerType = long long,
    typename FloatType = double,
    typename BooleanType = bool,
    typename CharType = typename StringType::value_type,
    typename StringPtrType = CharType const*>
class JsonDocument
{
public:
    typedef JsonValue<StringType, IntegerType, FloatType, BooleanType> json_value_type;

    typedef typename json_value_type::JsonType JsonType;

    // Character type supported for JSON strings.
    typedef typename json_value_type::char_type char_type;

    // Return type for string::c_str.
    typedef typename json_value_type::string_ptr_type string_ptr_type;

    // JSON types that are not an array or object.
    typedef typename json_value_type::string_type string_type;
    typedef typename json_value_type::integer_type integer_type;
    typedef typename json_value_type::float_type float_type;
    typedef typename json_value_type::boolean_type boolean_type;

    JsonDocument() noexcept
    {
    }

    JsonDocument(std::string const& filename)
    {
        if (!Load(filename))
            m_head.SetType(JsonType::Error);
    }

    ~JsonDocument()
    {
    }

    operator json_value_type const&() const noexcept
    {
        return Value();
    }

    operator json_value_type&() noexcept
    {
        return Value();
    }

    // Import a string containing JSON values and parse.
    bool Parse(string_type const& buf)
    {
        return buf.empty() ? false : m_head.Parse(buf);
    }

    // Import a string containing JSON values and parse.
    bool Parse(string_ptr_type buf)
    {
        return buf && *buf != '\0' && m_head.Parse(buf);
    }

    // Load a JSON values from a JSON file.
    bool Load(string_type const& filename)
    {
        char* buf = ReadFile(filename);
        bool success = buf && Parse(buf);
        delete[] buf;
        return success;
    }

    // Save the JSON values into a JSON file.
    bool Save(string_type const& filename)
    {
        string_type buf = m_head.ToJsonString();
        bool success = !buf.empty() && WriteFile(filename, buf);
        return success;
    }

    // Save the JSON values into a JSON file.
    bool Save(string_type const& filename,
              bool prettify,
              std::size_t column = 0,
              std::size_t indent = 4,
              string_ptr_type eol = InternalStringUtility<char_type>::eol())
    {
        string_type buf = m_head.ToJsonString(prettify, column, indent, eol);
        bool success = !buf.empty() && WriteFile(filename, buf);
        return success;
    }

    json_value_type& Value() noexcept
    {
        return m_head;
    }

    json_value_type const& Value() const noexcept
    {
        return m_head;
    }

    void SetHead(json_value_type const& head)
    {
        m_head.Copy(head);
    }

    void SetHead(json_value_type&& head) noexcept(false)
    {
        m_head.Move(head);
    }

    // Read a file and return a pointer allocated with new[]
    static char* ReadFile(std::string const& filename)
    {
        char* buf = nullptr;
        std::FILE* fp = OpenFile(filename.c_str(), "rb");

        if (fp)
        {
            if (!std::fseek(fp, 0, SEEK_END))
            {
                long size = std::ftell(fp);
                if (size > 0 && !std::fseek(fp, 0, SEEK_SET))
                {
                    buf = new char[size + 1];
                    std::size_t bytes_read = std::fread(buf, 1U, static_cast<std::size_t>(size), fp);
                    if (bytes_read != static_cast<std::size_t>(size))
                    {
                        delete[] buf;
                        buf = nullptr;
                    }
                    else
                        buf[size] = '\0';
                }
            }
            std::fclose(fp);
        }

        return buf;
    }

    // Write the buffer to a file.
    static bool WriteFile(std::string const& filename, string_ptr_type buf, std::size_t size)
    {
        bool success = false;

        if (buf && size > 0U)
        {
            std::FILE* fp = OpenFile(filename.c_str(), "wb");
            if (fp)
            {
                std::fwrite(buf, sizeof(buf[0]), size, fp);
                std::fclose(fp);
                success = true;
            }
        }

        return success;
    }

    // Write the buffer to a file.
    static bool WriteFile(std::string const& filename, string_type const& buf)
    {
        return WriteFile(filename, buf.c_str(), buf.size());
    }

private:
    static FILE* OpenFile(char const* filename, char const* mode)
    {
#if (defined(WIN32) || defined(_WIN32)) && !defined(_CRT_SECURE_NO_WARNINGS)
        std::FILE* fp = nullptr;
        errno_t err = fopen_s(&fp, filename, mode);
        if (err)
            fp = nullptr;
#else
        std::FILE* fp = std::fopen(filename, "rb");
#endif
        return fp;
    }

    // The head can be any data type, array or object.
    // When the head is a string, integer, float or boolean then there are no other values.
    json_value_type m_head;
};

// json namespace provides the default JsonDocument using
// std::string, long long, double and bool for JSON string, integer, float and boolean.
namespace json
{
    typedef JsonDocument<> Document;
}

} // namespace ocl

#endif // OCL_GUARD_JSONDOCUMENT_HPP
