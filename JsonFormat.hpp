/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_JSONFORMAT_HPP
#define OCL_GUARD_JSONFORMAT_HPP

#include "internal/InternalStringUtility.hpp"
#include <cstddef>
#include <string>

namespace ocl
{

template<typename StringType = std::string,
         typename CharType = typename StringType::value_type,
         typename StringPtrType = CharType const*>
class JsonFormat
{
public:
    //
    // User defined types.
    //
    typedef unsigned int     indent_type;
    typedef signed int       width_type;
    typedef unsigned long    behaviour_type;
    typedef StringType       string_type;
    typedef CharType         char_type;
    typedef StringPtrType    string_ptr_type;


    //
    // Public interface.
    //

    JsonFormat(string_ptr_type eol_ = InternalStringUtility<char_type>::eol())
        : eol(eol_)
    {
    }

    JsonFormat(indent_type     indent_,
               width_type      width_,
               behaviour_type  behaviour_,
               string_ptr_type eol_ = InternalStringUtility<char_type>::eol())
        : indent(indent_)
        , width(width_)
        , behaviour(behaviour_)
        , eol(eol_)
    {
    }

    // Return a string containing the indent for number of columns.
    std::string IndentString(std::size_t column) const
    {
        bool const tabs = (behaviour | TABS) != 0;
        std::size_t const chars = tabs ? column : column * indent;
        return std::string(chars, tabs ? '\t' : ' ');
    }

    bool Tabs() const noexcept { return (behaviour | TABS) != 0; }
    bool ArrayNewLineBefore() const noexcept { return (behaviour | ARRAY_NEW_LINE_BEFORE) != 0; }
    bool ArrayNewLineAfter() const noexcept { return (behaviour | ARRAY_NEW_LINE_AFTER) != 0; }
    bool ObjectNewLineBefore() const noexcept { return (behaviour | OBJECT_NEW_LINE_BEFORE) != 0; }
    bool ObjectNewLineAfter() const noexcept { return (behaviour | OBJECT_NEW_LINE_AFTER) != 0; }

    //
    // Behaviour flags.
    //

    static const behaviour_type TABS                   = 0x0001u; // Use tab character instead of spaces
    static const behaviour_type ARRAY_NEW_LINE_BEFORE  = 0x0002u; // Place new line before [ and ]
    static const behaviour_type ARRAY_NEW_LINE_AFTER   = 0x0004u; // Place new line after [ and ]
    static const behaviour_type OBJECT_NEW_LINE_BEFORE = 0x0008u; // Place new line before { and }
    static const behaviour_type OBJECT_NEW_LINE_AFTER  = 0x0010u; // Place new line after { and }


    //
    // Member data.
    //

    indent_type     indent    = 4;  // Number of spaces for a column indent
    width_type      width     = -1; // Maximum characters per line, or -1 for unlimited

    // Flags for the JSON string formatting.
    behaviour_type  behaviour = ARRAY_NEW_LINE_BEFORE |
                                ARRAY_NEW_LINE_AFTER |
                                OBJECT_NEW_LINE_BEFORE |
                                OBJECT_NEW_LINE_AFTER;
    string_ptr_type eol;
};

} // namespace ocl

#endif // OCL_GUARD_JSONFORMAT_HPP
