/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_INERNAL_INTERNALJSONSTRINGPARSER_HPP
#define OCL_GUARD_INERNAL_INTERNALJSONSTRINGPARSER_HPP

#include "InternalTypeConversion.hpp"

namespace ocl
{

// Parser JSON a string to extract the data for a JsonValue.
// E.g. Parser "hello" to extract a std::string containing the text hello or 
template<typename JsonType>
class InternalJsonStringParser;

template<>
class InternalJsonStringParser<std::string>
{
public:
    typedef std::string::size_type size_type;
    typedef char char_type;
    typedef char const* string_ptr_type;
    typedef std::string string_type;

    static const char_type quote_char = '"';
    static const char_type null_char  = '\0';

    // When the string pointer starts with '"' the string is extracted into the returning value.
    // If '"' is not found before the end of the string then null is returned and value is not set.
    static string_ptr_type Extract(string_ptr_type s, string_type& value)
    {
        if (*s == quote_char)
        {
            bool is_set = false;
            ++s;
            string_ptr_type start = s;

            for (char ch = *s; ch != null_char;)
            {
                if (ch == quote_char)
                {
                    value.assign(start, s - start);
                    ++s;
                    is_set = true;
                    break;
                }
                ++s;
                ch = *s;
            }

            if (!is_set)
                value.clear();
        }
        else
            value.clear();

        return s;
    }

    // When the string pointer starts with '"' the string is extracted into the returning value.
    // If '"' is not found before the end of the string then null is returned and value is not set.
    static string_ptr_type Extract(string_ptr_type s, string_ptr_type& start, std::size_t& count)
    {
        if (*s == quote_char)
        {
            ++s;
            start = s;

            for (char ch = *s; ch != null_char;)
            {
                if (ch == quote_char)
                {
                    count = s - start;
                    ++s;
                    break;
                }
                else
                {
                    ++s;
                    ch = *s;
                }
            }
        }

        return s;
    }
};

template<>
class InternalJsonStringParser<long long>
{
public:
    typedef char char_type;
    typedef char const* string_ptr_type;
    typedef long long integer_type;
    typedef double    float_type;

    // When the string pointer starts with '0'..'9' the string is extracted into the returning integer value.
    // If '"' is not found before the end of the string then null is returned and value is not set.
    static char const* Extract(char const* s, integer_type& integer_value, float_type& float_value, bool& is_integer)
    {
        string_ptr_type next = nullptr;

        if (s)
        {
            is_integer = true;
            string_ptr_type start = s;
            char_type ch = *s;

            while (!next)
            {
                switch (ch)
                {
                case '-':
                    if (s > start)
                        return nullptr; // Minus sign can only be the first character.
                    break;
                // Check for integer and float
                case '0':
                    [[fallthrough]];
                case '1':
                    [[fallthrough]];
                case '2':
                    [[fallthrough]];
                case '3':
                    [[fallthrough]];
                case '4':
                    [[fallthrough]];
                case '5':
                    [[fallthrough]];
                case '6':
                    [[fallthrough]];
                case '7':
                    [[fallthrough]];
                case '8':
                    [[fallthrough]];
                case '9':
                    ++s;
                    ch = *s;
                    break;
                case '.':
                    if (!is_integer)
                        return nullptr; // Cannot have two decimal points in a number.
                    is_integer = false;
                    ++s;
                    ch = *s;
                    break;
                default:
                    if (is_integer)
                    {
                        if (s > start) // Check for at least one digit.
                        {
                            next = s;
                            integer_value = InternalTypeConversion<char const*, integer_type>::Convert(start, s - 1);
                        }
                        else
                            return nullptr; // Must have at least one digit for integer value.
                    }
                    else if (s > start + 1) // Check for at least one digit and one decimal place.
                    {
                        next = s;
                        float_value = InternalTypeConversion<char const*, float_type>::Convert(start);
                    }
                    else
                        return nullptr; // Must have at least one digit and decimal point for float value.
                    break;
                }
            }
        }

        return next;
    }
};

} // namespace ocl

#endif // OCL_GUARD_INERNAL_INTERNALJSONSTRINGPARSER_HPP
