/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_INTERNAL_INTERNALSTRINGUTILITY_HPP
#define OCL_GUARD_INTERNAL_INTERNALSTRINGUTILITY_HPP

#include <cstddef>

namespace ocl
{

template<typename CharType = char>
class InternalStringUtility
{
public:
    typedef CharType char_type;
    typedef std::string string_type;
    typedef CharType const* string_ptr_type;

    static char const* eol() noexcept
    {
#if defined(WIN32) || defined(_WIN32)
        static char const* EOL = "\r\n";
#else
        static char const* EOL = "\n";
#endif
        return EOL;
    }

    // Check for a valid white space JSON character.
    static bool IsWhiteSpace(char_type ch) noexcept
    {
        return ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t';
    }

    // Check for a white space or comma which can be between JSON array or JSON object values.
    static bool IsNextValueSeparator(char_type ch) noexcept
    {
        return ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t' || ch == ',';
    }

    static bool IsInteger(char_type ch)
    {
        return ch >= '0' && ch <= '9';
    }

    static bool IsNumeric(char_type ch)
    {
        return (ch >= '0' && ch <= '9') || ch == '.';
    }

    // Skip white recognised space for the JSON string.
    static string_ptr_type SkipWhiteSpace(string_ptr_type json_string) noexcept
    {
        while (IsWhiteSpace(*json_string))
            ++json_string;
        return json_string;
    }

    // Skip all characters between two JSON array or JSON object values.
    static string_ptr_type FindNextValue(string_ptr_type json_string) noexcept
    {
        while (IsNextValueSeparator(*json_string))
            ++json_string;
        return json_string;
    }

    // Implement strcmp equivalent to remove MSVC strcmp_s compiler warning.
    static bool Compare(string_ptr_type s1, string_ptr_type s2)
    {
        while (*s1 == *s2)
        {
            if (*s1 == '\0')
                return true;
            ++s1;
            ++s2;
        }
        return *s1 == *s2;
    }

    // Implement strcmp equivalent to remove MSVC strcmp_s compiler warning.
    static bool Compare(string_ptr_type s1, string_ptr_type s2, std::size_t size)
    {
        string_ptr_type end = s1 + size;
        while (s1 < end)
        {
            if (*s1 != *s2)
                return false;
            ++s1;
            ++s2;
        }
        return true;
    }
};

} // namespace ocl

#endif // OCL_GUARD_INTERNAL_INTERNALSTRINGUTILITY_HPP

