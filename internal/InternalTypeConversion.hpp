/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_INERNAL_INTERNALTYPECONVERSION_HPP
#define OCL_GUARD_INERNAL_INTERNALTYPECONVERSION_HPP

#include "StdIntTraits.hpp"
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>

namespace ocl
{

template<typename FromType, typename ToType>
class InternalTypeConversion;

//
// String to string, integer, float and boolean.
//

template<>
class InternalTypeConversion<std::string, std::string>
{
public:
    static std::string const& Convert(std::string const& s) noexcept
    {
        return s;
    }
};

template<>
class InternalTypeConversion<std::string, long long>
{
public:
    static long long Convert(std::string const& s)
    {
        return std::stoll(s);
    }

    static long long Convert(char const* s)
    {
        char* end{};
        return std::strtoll(s, &end, 10);
    }
};

template<>
class InternalTypeConversion<std::string, double>
{
public:
    static double Convert(std::string const& s)
    {
        char* end{};
        return std::strtod(s.c_str(), &end);
    }

    static double Convert(char const* s)
    {
        char* end{};
        return std::strtod(s, &end);
    }
};

template<>
class InternalTypeConversion<std::string, bool>
{
public:
    static bool Convert(std::string const& s) noexcept
    {
        return s == "true";
    }

    static bool Convert(char const*s) noexcept
    {
        static const char* t = "true";
        return ::memcmp(s, t, 5) == 0;
    }
};

//
// String (char const*) to string, integer, float and boolean.
//

template<>
class InternalTypeConversion<char const*, std::string>
{
public:
    static std::string Convert(char const* s)
    {
        return s;
    }
};

template<>
class InternalTypeConversion<char const*, char const*>
{
public:
    static char const* Convert(char const* s) noexcept
    {
        return s;
    }
};

template<>
class InternalTypeConversion<char const*, long long>
{
public:
    static long long Convert(char const* s)
    {
        char* end{};
        return std::strtoll(s, &end, 10);
    }

    static long long Convert(char const* s, char const* end)
    {
        long long value;
        long long total = 0;
        char const* start = end;
        for (; end >= s; --end)
        {
            char ch = *end;
            switch (ch)
            {
            case '0':
                [[fallthrough]];
            case '1':
                [[fallthrough]];
            case '2':
                [[fallthrough]];
            case '3':
                [[fallthrough]];
            case '4':
                [[fallthrough]];
            case '5':
                [[fallthrough]];
            case '6':
                [[fallthrough]];
            case '7':
                [[fallthrough]];
            case '8':
                [[fallthrough]];
            case '9':
                value = static_cast<long long>(ch - '0');
                if (start == end)
                    total = value;
                else
                    total += StdIntTraits<long long>::Multiplier(start - end) * value;
                break;
            case '-':
                total = -total;
                break;
            default:
                return 0LL;
            }
        }

        return total;
    }
};

template<>
class InternalTypeConversion<char const*, double>
{
public:
    static double Convert(char const* s)
    {
        char* end{};
        return std::strtod(s, &end);
    }

    static double Convert(char const* s, char const* /*end*/)
    {
        // TODO: look for optimisations to replace strtod.
        char* e{};
        return std::strtod(s, &e);
    }
};

template<>
class InternalTypeConversion<char const*, bool>
{
public:
    static bool Convert(char const* s) noexcept
    {
        static const char* t = "true";
        return ::memcmp(s, t, 5) == 0;
    }
};

//
// Integer to string, integer, float and boolean.
//

template<>
class InternalTypeConversion<long long, std::string>
{
public:
    static std::string Convert(long long l)
    {
        return std::to_string(l);
    }
};

template<>
class InternalTypeConversion<long long, long long>
{
public:
    static long long Convert(long long l) noexcept
    {
        return l;
    }
};

template<>
class InternalTypeConversion<long long, double>
{
public:
    static double Convert(long long l) noexcept
    {
        return static_cast<double>(l);
    }
};

template<>
class InternalTypeConversion<long long, bool>
{
public:
    static bool Convert(long long l) noexcept
    {
        return l != 0ll;
    }
};

//
// Float to string, integer, float and boolean.
//

template<>
class InternalTypeConversion<double, std::string>
{
public:
    static std::string Convert(double d)
    {
        return std::to_string(d);
    }
};

template<>
class InternalTypeConversion<double, long long>
{
public:
    static long long Convert(double d)
    {
        double num = 0;
        (void)std::modf(d, &num);
        return static_cast<long long>(num);
    }
};

template<>
class InternalTypeConversion<double, double>
{
public:
    static double Convert(double d) noexcept
    {
        return d;
    }
};

template<>
class InternalTypeConversion<double, bool>
{
public:
    static bool Convert(double d)
    {
        double num = 0;
        (void)std::modf(d, &num);
        return static_cast<long long>(num) != 0;
    }
};

//
// Boolean to string, integer, float and boolean.
//

template<>
class InternalTypeConversion<bool, std::string>
{
public:
    static std::string Convert(bool b) noexcept
    {
        return b ? "true" : "false";
    }
};

template<>
class InternalTypeConversion<bool, long long>
{
public:
    static long long Convert(bool b) noexcept
    {
        return b ? 1ll : 0ll;
    }
};

template<>
class InternalTypeConversion<bool, double>
{
public:
    static double Convert(bool b) noexcept
    {
        return b ? 1.0 : 0.0;
    }
};

template<>
class InternalTypeConversion<bool, bool>
{
public:
    static bool Convert(bool b) noexcept
    {
        return b;
    }
};

} // namespace ocl

#endif // OCL_GUARD_INERNAL_INTERNALTYPECONVERSION_HPP
