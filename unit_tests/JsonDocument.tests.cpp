/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../JsonDocument.hpp"

#include <functional>
#include <string>

constexpr const bool test_int = false;

TEST_MEMBER_FUNCTION(JsonDocument, Load, string)
{
    std::string files[]
    {
        "json_array_test.json", // File containing array with values 1 and 2.
        "json_boolean_test.json",
        "json_empty_array_test.json",
        "json_float_test.json",
        "json_integer_test.json",
        "json_null_test.json",
        "json_object_test.json",
        "json_string_test.json"
    };

    // typedef the enum containing supported JSON types.
    typedef ocl::json::JsonType JsonType;

    typedef ocl::json::Document json_document_type;
    typedef ocl::json::Value value_type;

    // Supported JSON types.
    typedef ocl::json::IntegerType integer_type;
    typedef ocl::json::FloatType float_type;
    typedef ocl::json::StringType string_type;
    typedef ocl::json::BooleanType boolean_type;

    // Test the JSON types and values for each loaded file.
    std::function<bool(json_document_type&)> type_checks[]
    {
        // Test JSON array with 2 elements.
        [](json_document_type& document)
        {
            value_type& value = document.Value();
            if (value.Type() == JsonType::Array)
            {
                return value.Size() == 2 &&
                       value[0ULL].GetInteger() == 1 &&
                       value[1ULL].GetInteger() == 2;
            }
            return false;
        },

        // Test JSON boolean.
        [](json_document_type& document)
        {
            value_type value = document.Value();
            return value.Type() == JsonType::Boolean &&
                   static_cast<boolean_type>(value) == true;
        },

        // Test empty JSON array.
        [](json_document_type& document)
        {
            value_type value = document.Value();
            return value.Type() == JsonType::Array && value.IsEmpty();
        },

        // Test JSON float equals 2.34
        [](json_document_type& document)
        {
            value_type value = document.Value();
            return value.Type() == JsonType::Float &&
                   static_cast<float_type>(value) == 2.34;
        },

        // Test JSON integer equals 35
        [](json_document_type& document)
        {
            value_type value = document.Value();
            return value.Type() == JsonType::Integer &&
                   static_cast<integer_type>(value) == 35;
        },

        // Test JSON value is null.
        [](json_document_type& document)
        {
            return document.Value().Type() == JsonType::Null;
        },

        // Test JSON value is an empty object.
        [](json_document_type& document)
        {
            value_type value = document.Value();
            return value.Type() == JsonType::Object && value.IsEmpty();
        },

        // Test JSON string is "hello"
        [](json_document_type& document)
        {
            value_type value = document.Value();
            return value.Type() == JsonType::String &&
                   static_cast<string_type const&>(value) == "hello";
        }
    };

#if defined(WIN32) || defined(_WIN32)
    std::string slash = "\\";
#else
    std::string slash = "/";
#endif

    std::size_t pos = 0;
    for (std::string const& filename : files)
    {
        json_document_type document;
        std::string pathname = "test_files" + slash + filename;
        CHECK_TRUE(document.Load(pathname));
        bool passed = (type_checks[pos])(document);
        if (!passed)
            passed = (type_checks[pos])(document); // Place breakpoint when failed here.
        CHECK_TRUE(passed);
        ++pos;
    }

    {
        std::string pathname = "test_files" + slash + "json_complete_test.json";
        json_document_type document;
        CHECK_TRUE(document.Load(pathname));

        try
        {
            value_type const& value = document.Value();
            CHECK_TRUE(value.Type() == JsonType::Object);

            value_type const& int_value = value["int_value"];
            CHECK_TRUE(int_value.Type() == JsonType::Integer);
            CHECK_TRUE((integer_type)int_value == 123);

            value_type const& float_value = value["float_value"];
            CHECK_TRUE(float_value.Type() == JsonType::Float);
            CHECK_TRUE((float_type)float_value == 45.67);

            value_type const& bool_value = value["bool_value"];
            CHECK_TRUE(bool_value.Type() == JsonType::Boolean);
            CHECK_TRUE((boolean_type)bool_value == true);

            value_type const& string_value = value["string_value"];
            CHECK_TRUE(string_value.Type() == JsonType::String);
            CHECK_TRUE((string_type const&)string_value == "A string value");

            value_type const& null_value = value["null_value"];
            CHECK_TRUE(null_value.IsNull());

            value_type const& small_list = value["small_list"];
            CHECK_TRUE(small_list.Type() == JsonType::Array);
            CHECK_TRUE(small_list[0ULL].GetInteger() == 234);
            CHECK_TRUE(small_list[1ULL].GetFloat() == 56.78);
            CHECK_TRUE(small_list[2ULL].GetBoolean() == false);
            CHECK_TRUE(small_list[3ULL].GetString() == "Another string value");
            CHECK_TRUE(small_list[4ULL].IsNull());

            value_type const& object_value = value["object"];
            CHECK_TRUE(object_value.Type() == JsonType::Object);
            CHECK_TRUE(object_value.LookupValue("a").GetInteger() == 345);
            CHECK_TRUE(object_value.LookupValue("b").GetFloat() == 67.89);
            CHECK_TRUE(object_value.LookupValue("c").GetBoolean() == true);
            CHECK_TRUE(object_value.LookupValue("d").GetString() == "dee");
            CHECK_TRUE(object_value.LookupValue("e").IsNull());
        }

        catch (std::exception&)
        {
            // Should never happen.
            CHECK_TRUE(false);
        }
    }
}

TEST_MEMBER_FUNCTION(JsonDocument, Parse, string)
{
    // typedef the enum containing valid JSON types.
    typedef ocl::json::JsonType JsonType;

    typedef ocl::json::Document json_document_type;
    typedef ocl::json::Value value_type;

    // Supported JSON types.
    typedef ocl::json::IntegerType integer_type;
    typedef ocl::json::FloatType float_type;
    typedef ocl::json::StringType string_type;
    typedef ocl::json::BooleanType boolean_type;
    typedef ocl::json::JsoStringPtrType string_ptr_type;

    string_ptr_type json =
        "{\n"
        "    \"int_value\": 123,\n"
        "   \"float_value\" : 45.67,\n"
        "   \"bool_value\" : true,\n"
        "   \"string_value\" : \"A string value\",\n"
        "   \"null_value\" : null,\n"
        "   \"small_list\" : [234, 56.78, false, \"Another string value\", null] ,\n"
        "   \"list_of_list\" : [\n"
        "       [ 1, 2 ],\n"
        "       [3.4, 5.6],\n"
        "       [false, true],\n"
        "       [\"first string\", \"second string\"],\n"
        "       [null, null]\n"
        "   ],\n"
        "   \"object\": {\n"
        "       \"a\": 345,\n"
        "       \"b\" : 67.89,\n"
        "       \"c\" : true,\n"
        "       \"d\" : \"dee\",\n"
        "       \"e\" : null\n"
        "   }\n"
        "}\n";

    string_type const int_value_str = "int_value";
    string_type const float_value_str = "float_value";
    string_type const bool_value_str = "bool_value";
    string_type const string_value_str = "string_value";
    string_type const null_value_str = "null_value";

    json_document_type document;
    CHECK_TRUE(document.Parse(json));

    try
    {
        value_type const& value = document.Value();
        CHECK_TRUE(value.Type() == JsonType::Object);

        value_type const& int_value = value[int_value_str];
        CHECK_TRUE(int_value.Type() == JsonType::Integer);
        CHECK_TRUE(int_value.GetInteger() == 123);

        value_type const& float_value = value[float_value_str];
        CHECK_TRUE(float_value.Type() == JsonType::Float);
        CHECK_TRUE(float_value.GetFloat() == 45.67);

        value_type const& bool_value = value[bool_value_str];
        CHECK_TRUE(bool_value.Type() == JsonType::Boolean);
        CHECK_TRUE(bool_value.GetBoolean() == true);

        value_type const& string_value = value[string_value_str];
        CHECK_TRUE(string_value.Type() == JsonType::String);
        CHECK_TRUE(string_value.GetString() == "A string value");

        value_type const& null_value = value[null_value_str];
        CHECK_TRUE(null_value.IsNull());

        value_type const& small_list = value["small_list"];
        CHECK_TRUE(small_list.Type() == JsonType::Array);
        CHECK_TRUE(small_list[0ULL].GetInteger() == 234);
        CHECK_TRUE(small_list[1ULL].GetFloat() == 56.78);
        CHECK_TRUE(small_list[2ULL].GetBoolean() == false);
        CHECK_TRUE(small_list[3ULL].GetString() == "Another string value");
        CHECK_TRUE(small_list[4ULL].IsNull());

        value_type const& object_value = value["object"];
        CHECK_TRUE(object_value.Type() == JsonType::Object);
        CHECK_TRUE(value["object"]["a"].GetInteger() == 345);
        CHECK_TRUE(object_value.LookupValue("a").GetInteger() == 345);
        CHECK_TRUE(object_value.LookupValue("b").GetFloat() == 67.89);
        CHECK_TRUE(object_value.LookupValue("c").GetBoolean() == true);
        CHECK_TRUE(object_value.LookupValue("d").GetString() == "dee");
        CHECK_TRUE(object_value.LookupValue("e").IsNull());
    }

    catch (std::exception&)
    {
        // Should never happen.
        CHECK_TRUE(false);
    }
}

#if __has_include("JsonDocument.performance.tests.cpp")
#include "JsonDocument.performance.tests.cpp"
#endif
