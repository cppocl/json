/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <exception>
#include <cstddef>
#include <string>

namespace ocl
{

class JsonException : public std::exception
{
public:
    // Generate a message.
    JsonException(char const* msg)
        : m_message(msg)
    {
    }

    // Generate a message with a position of where the error occurred.
    JsonException(char const* msg, std::size_t stream_position)
        : m_message(msg)
    {
        m_message += ": ";
        m_message += std::to_string(stream_position);
    }

    // Generate a message that contains the string to report.
    // Up to max_len characters will be used from the string position.
    JsonException(char const* msg, char const* string_position, std::size_t max_len = 30)
        : m_message(msg)
    {
        std::size_t len = 0;
        char const* s = string_position;

        for (; *s != '\0'; ++s)
            if (len == max_len)
                break;
            else
                ++len;

        m_message += ": ";
        m_message += std::string(string_position, len);

        if (s[len] != '\0')
            m_message += "..."; // String was longer than m_max_len so indicate there is more.
    }

    // Return the error message.
    char const* what() const override
    {
        return m_message.c_str();
    }

private:
    std::string m_message;
};

} // namespace ocl
