/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_JSONVALUE_HPP
#define OCL_GUARD_JSONVALUE_HPP

#include "internal/InternalTypeConversion.hpp"
#include "internal/InternalJsonStringParser.hpp"
#include "internal/InternalStringUtility.hpp"
#include "internal/IntMinMax.hpp"
#include "JsonFormat.hpp"
#include "JsonException.hpp"
#include <cmath>
#include <map>
#include <limits>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

namespace ocl
{

// JSON value class, which supports all the types and conversions between them.
template<typename StringType = std::string,
         typename IntegerType = long long,
         typename FloatType = double,
         typename BooleanType = bool>
class JsonValue
{
public:
    // Enumeration of supported JSON types.
    enum class JsonType : int
    {
        Error = -1,   // Internal read-only error type
        Null = 0,
        String,
        Integer,
        Float,
        Boolean,
        Array,
        Object // A key/value map
    };

    typedef JsonValue<StringType, IntegerType, FloatType, BooleanType> json_value_type;

    // JSON primitive types.
    typedef StringType  string_type;
    typedef IntegerType integer_type;
    typedef FloatType   float_type;
    typedef BooleanType boolean_type;

    // Character type supported for JSON strings.
    typedef typename string_type::value_type char_type;

    // Return type for string::c_str.
    typedef typename string_type::const_pointer string_ptr_type;

    // Format settings for JSON string output.
    typedef JsonFormat<string_type, char_type, string_ptr_type> json_format_type;

private:
    // JSON array of JSON values.
    typedef std::vector<json_value_type*> array_type;

    // JSON object is a collection of unique JSON string key and JSON value as pairs.
    typedef std::map<string_type, json_value_type*> object_type;

    // Pair type for the JSON object type.
    typedef std::pair<string_type, json_value_type*> object_pair_type;

public:
    //
    // Construction and destruction.
    //

    // Construct a default JSON null value.
    JsonValue() noexcept
    {
    }

    // Construct an empty JSON value.
    JsonValue(JsonType type)
        : m_type(type)
    {
        InternalSetData(type);
    }

    // Construct and copy JSON value.
    JsonValue(json_value_type const& value)
    {
        InternalCopy(value);
    }

    // Construct and move JSON value.
    JsonValue(json_value_type&& value) noexcept
    {
        InternalMove(value);
    }

    // Construct string JSON value.
    JsonValue(const char* s)
        : m_type(JsonType::String)
        , m_string(s ? new string_type(s) : new string_type)
    {
    }

    // Construct string JSON value.
    JsonValue(string_type const& s)
        : m_type(JsonType::String)
        , m_string(new string_type(s))
    {
    }

    // Construct and move string JSON value.
    JsonValue(string_type&& s) noexcept(false)
        : m_type(JsonType::String)
        , m_string(new string_type(std::move(s)))
    {
    }

    // Construct integer JSON value.
    JsonValue(short value)
        : m_type(JsonType::Integer)
        , m_integer(value)
    {
    }

    // Construct integer value.
    JsonValue(int value)
        : m_type(JsonType::Integer)
        , m_integer(value)
    {
    }

    // Construct integer value.
    JsonValue(long value)
        : m_type(JsonType::Integer)
        , m_integer(value)
    {
    }

    // Construct integer value.
    JsonValue(integer_type value)
        : m_type(JsonType::Integer)
        , m_integer(value)
    {
    }

    // Construct float value.
    JsonValue(float_type value)
        : m_type(JsonType::Float)
        , m_float(value)
    {
    }

    // Construct boolean value.
    JsonValue(boolean_type value)
        : m_type(JsonType::Boolean)
        , m_boolean(value)
    {
    }

    // Construct JSON array.
    JsonValue(array_type const& a)
    {
        InternalCopy(a);
    }

    // Construct  and move JSON array.
    JsonValue(array_type&& a) noexcept(false)
    {
        InternalMove(a);
    }

    // Construct JSON object.
    JsonValue(object_type const& obj)
    {
        InternalCopy(obj);
    }

    // Construct and move JSON object.
    JsonValue(object_type&& obj) noexcept(false)
    {
        InternalMove(obj);
    }

    // Clear and destroy JSON value.
    ~JsonValue()
    {
        Clear();
    }


    //
    // Operators.
    //

    // Copy JSON value.
    json_value_type& operator=(json_value_type const& value)
    {
        Copy(value);
        return *this;
    }

    // Move JSON value.
    json_value_type& operator=(json_value_type&& value) noexcept
    {
        Move(value);
        return *this;
    }

    json_value_type& operator=(integer_type value) noexcept
    {
        if (!IsReadOnly())
        {
            Clear();
            m_type = JsonType::Integer;
            m_integer = value;
        }
        return *this;
    }

    json_value_type& operator=(float_type value) noexcept
    {
        if (!IsReadOnly())
        {
            Clear();
            m_type = JsonType::Float;
            m_float = value;
        }
        return *this;
    }

    json_value_type& operator=(boolean_type value)
    {
        CheckNotReadOnly();
        Clear();
        m_type = JsonType::Boolean;
        m_float = value;
        return *this;
    }

    json_value_type& operator=(array_type const& a)
    {
        CheckNotReadOnly();
        Clear();
        InternalCopy(a);
        return *this;
    }

    json_value_type& operator=(array_type&& a) noexcept(false)
    {
        CheckNotReadOnly();
        Clear();
        InternalMove(a);
        return *this;
    }

    json_value_type& operator=(object_type const& obj)
    {
        CheckNotReadOnly();
        Clear();
        InternalCopy(obj);
        return *this;
    }

    json_value_type& operator=(object_type&& obj) noexcept(false)
    {
        CheckNotReadOnly();
        Clear();
        InternalMove(obj);
        return *this;
    }

    bool operator==(json_value_type const& rhs) const noexcept
    {
        return Compare(rhs);
    }

    operator string_type const&() const
    {
        return GetString();
    }

    operator integer_type() const
    {
        if (Type() != JsonType::Integer)
            ThrowException("Not an integer");
        return m_integer;
    }

    operator float_type() const
    {
        if (Type() != JsonType::Float)
            ThrowException("Not a float");
        return m_float;
    }

    operator boolean_type() const
    {
        if (Type() != JsonType::Boolean)
            ThrowException("Not a boolean");
        return m_boolean;
    }

    // Find the value for the index within the array,
    // or throw an exception if the type is not an array or out of bounds.
    json_value_type& operator[](signed char index) { return InternalIndexAt(index); }
    json_value_type const& operator[](signed char index) const { return InternalIndexAt(index); }
    json_value_type& operator[](unsigned short index) { return InternalIndexAt(index); }
    json_value_type const& operator[](unsigned short index) const { return InternalIndexAt(index); }
    json_value_type& operator[](unsigned int index) { return InternalIndexAt(index); }
    json_value_type const& operator[](unsigned int index) const { return InternalIndexAt(index); }
    json_value_type& operator[](unsigned long int index) { return InternalIndexAt(index); }
    json_value_type const& operator[](unsigned long int index) const { return InternalIndexAt(index); }
    json_value_type& operator[](unsigned long long int index) { return InternalIndexAt(index); }
    json_value_type const& operator[](unsigned long long int index) const { return InternalIndexAt(index); }

    // Find the value for the key in a JSON object,
    // or throw an exception if the type is not an object.
    json_value_type& operator[](string_type const& key)
    {
        if (Type() != JsonType::Object)
            ThrowException("Not an object");

        auto iter = m_map->find(key);

        if (iter == m_map->end())
        {
            std::string msg("Key not found: ");
            msg += key;
            ThrowException(msg.c_str());
        }

        return *iter->second;
    }

    // Find the value for the key in a JSON object,
    // or throw an exception if the type is not an object.
    json_value_type const& operator[](string_type const& key) const
    {
        return const_cast<json_value_type&>(*this).operator[](key);
    }

    // Find the value for the key in a JSON object,
    // or throw an exception if the type is not an object.
    json_value_type& operator[](string_ptr_type key)
    {
        return this->operator[](string_type(key));
    }

    // Find the value for the key in a JSON object,
    // or throw an exception if the type is not an object.
    json_value_type& operator[](string_ptr_type key) const
    {
        return const_cast<json_value_type&>(*this).operator[](key);
    }

    //
    // Interface functions.
    //

    // Return true when the JSON value is read-only.
    // Currently only the error type is read-only.
    bool IsReadOnly() const noexcept
    {
        return Type() == JsonType::Error;
    }

    // Return true if JSON value is an error.
    bool IsError() const noexcept
    {
        return Type() == JsonType::Error;
    }

    // Return true if JSON value is null, otherwise the JSON value is set.
    bool IsNull() const noexcept
    {
        return Type() == JsonType::Null;
    }

    // Return true if the JSON value is a string type.
    bool IsString() const noexcept
    {
        return Type() == JsonType::String;
    }

    // Return true if the JSON value is an integer type.
    bool IsInteger() const noexcept
    {
        return Type() == JsonType::Integer;
    }

    // Return true if the JSON value is a float type.
    bool IsFloat() const noexcept
    {
        return Type() == JsonType::Float;
    }

    // Return true if the JSON value is a boolean type.
    bool IsBoolean() const noexcept
    {
        return Type() == JsonType::Boolean;
    }

    // Return true if the JSON value is an array type.
    bool IsArray() const noexcept
    {
        return Type() == JsonType::Array;
    }

    // Return true if the JSON value is an object type.
    bool IsObject() const noexcept
    {
        return Type() == JsonType::Object;
    }

    // Return the JSON type for the JSON value.
    // This will be none, string, integer, float, boolean, array or object.
    JsonType Type() const noexcept
    {
        return m_type;
    }

    // Return the size of the array, or return 0 if the type is not an array.
    std::size_t Size() const
    {
        if (m_type == JsonType::Array)
            return m_array->size();
        if (m_type != JsonType::Object)
            ThrowException("Not an array or object");
        return m_map->size();
    }

    // Return the size of the array, or return 0 if the type is not an array.
    bool IsEmpty() const
    {
        if (m_type == JsonType::Array)
            return m_array->empty();
        if (m_type != JsonType::Object)
            ThrowException("Not an array or object");
        return m_map->empty();
    }

    // Throw an error and provide a single place for a break point.
    [[noreturn]] void ThrowException(char const* msg) const
    {
        throw JsonException(msg);
    }

    // Throw an error and provide a single place for a break point.
    [[noreturn]] void ThrowException(char const* msg, std::size_t stream_position) const
    {
        throw JsonException(msg, stream_position);
    }

    // Throw an error and provide a single place for a break point.
    [[noreturn]] void ThrowException(char const* msg, char const* string_position, std::size_t max_len = 30) const
    {
        throw JsonException(msg, string_position, max_len);
    }

    // Throw an error for when trying to modify a value that is expected to be null.
    // NOTE: The common case is placed here for the change of the message in the future.
    [[noreturn]] void ThrowNotNullException(string_ptr_type string_position) const
    {
        ThrowException("JSON value is already set, must be null", string_position);
    }

    // Check that the type is not an error, as this type is read-only.
    void CheckNotReadOnly()
    {
        if (IsReadOnly())
            ThrowException("Error type is reserved for internal use only");
    }

    // Validate indexing of the array and throw an exception on failure.
    template<typename IntType>
    void CheckArrayIndex(IntType index)
    {
        if (Type() != JsonType::Array)
            ThrowException("Not an array");

        if constexpr (sizeof(IntType) > sizeof(std::size_t))
            if (index > static_cast<IntType>(IntMinMax<std::size_t>::MaxValue))
                ThrowException("Index range too big");

        if constexpr (std::is_signed_v<IntType>)
        {
            if (index < static_cast<IntType>(0))
                ThrowException("Negative array index");
        }
        else if (static_cast<std::size_t>(index) >= m_array->size())
            ThrowException("Index out of bounds");
    }

    // Get the string value when the type is a string or throw an exception.
    string_type& GetString()
    {
        if (Type() != JsonType::String)
            ThrowException("Not a string");

        return *m_string;
    }

    // Get the string value when the type is a string or throw an exception.
    string_type const& GetString() const
    {
        return const_cast<json_value_type&>(*this).GetString();
    }

    // Get the integer value when the type is an integer or throw an exception.
    integer_type GetInteger() const
    {
        if (Type() != JsonType::Integer)
            ThrowException("Not an integer");

        return m_integer;
    }

    // Get the float value when the type is a float or throw an exception.
    float_type GetFloat() const
    {
        if (Type() != JsonType::Float)
            ThrowException("Not a float");

        return m_float;
    }

    // Get the boolean value when the type is a boolean or throw an exception.
    boolean_type GetBoolean() const
    {
        if (Type() != JsonType::Boolean)
            ThrowException("Not a boolean");

        return m_boolean;
    }

    // Get the array when the type is an array or throw an exception.
    array_type& GetArray()
    {
        if (Type() != JsonType::Array)
            ThrowException("Not an array");

        return *m_array;
    }

    // Get the array when the type is an array or throw an exception.
    array_type const& GetArray() const
    {
        return const_cast<json_value_type&>(*this).GetArray();
    }

    // Get the object when the type is a JSON object or throw an exception.
    object_type& GetObject()
    {
        if (Type() != JsonType::Object)
            ThrowException("Not an object");

        return *m_map;
    }

    // Get the object when the type is a JSON object or throw an exception.
    object_type const& GetObject() const
    {
        return const_cast<json_value_type&>(*this).GetObject();
    }

    // Get the value for the key from the JSON object.
    // Throw an exception if the JSON value is not an object or the key is not found.
    json_value_type& LookupValue(string_type const& key)
    {
        if (Type() != JsonType::Object)
            ThrowException("Not an object");

        auto iter = m_map->find(key);

        if (iter == m_map->end())
            ThrowException("Value not found");

        return *iter->second;
    }

    // Get the value for the key from the JSON object.
    // Throw an exception if the JSON value is not an object or the key is not found.
    json_value_type const& LookupValue(string_type const& key) const
    {
        return const_cast<json_value_type&>(*this).LookupValue(key);
    }

    // Get a copy of the string for a JSON string value.
    bool Get(string_type& value) const
    {
        bool success = IsString();
        if (success)
            value = *m_string;
        return success;
    }

    // Get a copy of the integer value.
    bool Get(integer_type& value) const
    {
        bool success = IsInteger();
        if (success)
            value = m_integer;
        return success;
    }

    // Get a copy of the float value.
    bool Get(float_type& value) const
    {
        bool success = IsFloat();
        if (success)
            value = m_float;
        return success;
    }

    // Get a copy of the boolean value.
    bool Get(boolean_type& value) const
    {
        bool success = IsBoolean();
        if (success)
            value = m_boolean;
        return success;
    }

    // Get a copy of the array.
    bool Get(array_type& value) const
    {
        bool success = IsArray();
        if (success)
            InternalCopy(value, *m_array);
        return success;
    }

    // Get a copy of the JSON object.
    bool Get(object_type& value) const
    {
        bool success = IsObject();
        if (success)
            InternalCopy(value, *m_map);
        return success;
    }

    // Get a copy of the JSON value.
    bool Get(json_value_type& value) const
    {
        value.Copy(*this);
        return true;
    }

    // Convert a string, integer, float, or boolean to a string.
    // An empty string will be an array, object or null.
    string_type ToString() const
    {
        string_type s;
        switch (m_type)
        {
        case JsonType::String:
            s = *m_string;
            break;
        case JsonType::Integer:
            s = InternalTypeConversion<integer_type, string_type>::Convert(m_integer);
            break;
        case JsonType::Float:
            s = InternalTypeConversion<float_type, string_type>::Convert(m_float);
            break;
        case JsonType::Boolean:
            s = InternalTypeConversion<boolean_type, string_type>::Convert(m_boolean);
            break;
        case JsonType::Error:
            [[fallthrough]];
        case JsonType::Null:
            [[fallthrough]];
        case JsonType::Array:
            [[fallthrough]];
        case JsonType::Object:
            [[fallthrough]];
        default:
            // Do nothing.
            break;
        }
        return s;
    }

    // Return the integer value, convert a string, float or boolean to integer value.
    // JSON null, array and object types return zero.
    integer_type ToInteger() const
    {
        integer_type l = 0;

        switch (m_type)
        {
        case JsonType::String:
            l = InternalTypeConversion<string_type, integer_type>::Convert(m_string->c_str());
            break;
        case JsonType::Integer:
            l = m_integer;
            break;
        case JsonType::Float:
            l = InternalTypeConversion<float_type, integer_type>::Convert(m_float);
            break;
        case JsonType::Boolean:
            l = InternalTypeConversion<boolean_type, integer_type>::Convert(m_boolean);
            break;
        case JsonType::Error:
            [[fallthrough]];
        case JsonType::Null:
            [[fallthrough]];
        case JsonType::Array:
            [[fallthrough]];
        case JsonType::Object:
            [[fallthrough]];
        default:
            // Do nothing.
            break;
        }

        return l;
    }

    // Return a float converted from types string, integer, float or boolean.
    // Boolean value will be converted to 0.0 for false or 1.0 for true.
    float_type ToFloat() const
    {
        float_type f = 0;

        switch (m_type)
        {
        case JsonType::String:
            f = InternalTypeConversion<string_type, float_type>::Convert(m_string->c_str());
            break;
        case JsonType::Integer:
            f = InternalTypeConversion<integer_type, float_type>::Convert(m_integer);
            break;
        case JsonType::Float:
            f = m_float;
            break;
        case JsonType::Boolean:
            f = InternalTypeConversion<boolean_type, float_type>::Convert(m_boolean);
            break;
        case JsonType::Error:
            [[fallthrough]];
        case JsonType::Null:
            [[fallthrough]];
        case JsonType::Array:
            [[fallthrough]];
        case JsonType::Object:
            [[fallthrough]];
        default:
            // Do nothing.
            break;
        }

        return f;
    }

    // Return a boolean converted from types string, integer, float or boolean.
    // String must be "true" or "false".
    // Integer will be zero for false, non-zero for true.
    // Float will be 0.0 for false, otherwise true.
    // Existing boolean value returned.
    boolean_type ToBoolean() const
    {
        boolean_type b = false;

        switch (m_type)
        {
        case JsonType::String:
            b = InternalTypeConversion<string_type, boolean_type>::Convert(m_string->c_str());
            break;
        case JsonType::Integer:
            b = m_integer == 0;
            break;
        case JsonType::Float:
            b = InternalTypeConversion<float_type, boolean_type>::Convert(m_float);
            break;
        case JsonType::Boolean:
            b = m_boolean;
            break;
        case JsonType::Error:
            [[fallthrough]];
        case JsonType::Null:
            [[fallthrough]];
        case JsonType::Array:
            [[fallthrough]];
        case JsonType::Object:
            [[fallthrough]];
        default:
            // Do nothing.
            break;
        }

        return b;
    }

    // Return an array with a copy the value for types string, integer, float, boolean and array.
    array_type ToArray() const
    {
        array_type a;

        switch (m_type)
        {
        case JsonType::String:
            a.push_back(new json_value_type(*m_string));
            break;
        case JsonType::Integer:
            a.push_back(new json_value_type(m_integer));
            break;
        case JsonType::Float:
            a.push_back(new json_value_type(m_float));
            break;
        case JsonType::Boolean:
            a.push_back(new json_value_type(m_boolean));
            break;
        case JsonType::Array:
            a.resize(m_array->size());
            for (typename array_type::size_type index = 0; index < m_array->size(); ++index)
                a[index] = (*m_array)[index]->Copy();
            break;
        case JsonType::Error:
            [[fallthrough]];
        case JsonType::Null:
            [[fallthrough]];
        case JsonType::Object:
            [[fallthrough]];
        default:
            // Do nothing.
            break;
        }

        return a;
    }

    // Change to string for types for integer, float and bool.
    // Existing null, array or object values will be lost and default to an empty string.
    void ChangeToString()
    {
        if (!IsString() && !IsReadOnly())
        {
            string_type s(ToString());
            Clear();
            InternalSetType(JsonType::String);
            *m_string = std::move(s);
        }
    }

    // Change to integer for types for string, float and bool.
    // Existing null, array or object values will be lost and default to zero.
    void ChangeToInteger()
    {
        if (!IsInteger() && !IsReadOnly())
        {
            Clear();
            integer_type i = ToInteger();
            InternalSetType(JsonType::Integer);
            m_integer = i;
        }
    }

    // Change to float for types for string, integer and bool.
    // Existing null, array or object values will be lost and default to 0.0.
    void ChangeToFloat()
    {
        if (!IsFloat() && !IsReadOnly())
        {
            float_type f = ToFloat();
            Clear();
            InternalSetType(JsonType::Float);
            m_float = f;
        }
    }

    // Change to boolean for types for string, integer and float.
    // Existing null, array or object values will be lost and default to false.
    void ChangeToBoolean()
    {
        if (!IsBoolean() && !IsReadOnly())
        {
            boolean_type b = ToBoolean();
            Clear();
            InternalSetType(JsonType::Boolean);
            m_boolean = b;
        }
    }

    // Change this value to an array, placing the existing value as the first element.
    void ChangeToArray()
    {
        if (!IsArray() && !IsReadOnly())
        {
            json_value_type* value = new json_value_type(std::move(this));
            InternalSetType(JsonType::Array);
            m_array->push_back(value);
        }
    }

    // Change this type to a JSON object, placing the existing value with the key.
    void ChangeToObject(string_type const& key)
    {
        if (!IsObject() && !IsReadOnly())
        {
            json_value_type* value = new json_value_type(std::move(this));
            InternalSetType(JsonType::Object);
            m_map->emplace(object_pair_type(key, value));
        }
    }

    // Convert the value to a JSON string.
    // JSON string is returned between " ",
    // JSON boolean is returned as true or false,
    // JSON array is returned between [ ] and , separating values,
    // JSON object is returned between { } with : used to separate key and value,
    // and , separating the key and value pairs.
    string_type ToJsonString() const
    {
        string_type s;

        switch (m_type)
        {
        case JsonType::Null:
            s = "null";
            break;
        case JsonType::String:
            s.reserve(m_string->size() + 2); // Reserve size + 2 for ""
            s = '"';
            s += *m_string;
            s += '"';
            break;
        case JsonType::Integer:
            s = InternalTypeConversion<integer_type, string_type>::Convert(m_integer);
            break;
        case JsonType::Float:
            s = InternalTypeConversion<float_type, string_type>::Convert(m_float);
            break;
        case JsonType::Boolean:
            s = InternalTypeConversion<boolean_type, string_type>::Convert(m_boolean);
            break;
        case JsonType::Array:
            InternalArrayToJsonString(s);
            break;
        case JsonType::Object:
            InternalObjectToJsonString(s);
            break;
        case JsonType::Error:
            [[fallthrough]];
        default:
            break;
        }

        return s;
    }

    // Convert the value to a JSON string with human-readable formatting.
    // JSON string is returned between " ",
    // JSON boolean is returned as true or false,
    // JSON array is returned between [ ] and , separating values,
    // JSON object is returned between { } with : used to separate key and value,
    // and , separating the key and value pairs.
    string_type ToJsonString(json_format_type const& fmt, std::size_t column = 0) const
    {
        string_type s;

        switch (m_type)
        {
        case JsonType::Null:
            s = "null";
            break;
        case JsonType::String:
            s.reserve(m_string->size() + 2); // Reserve size + 2 for ""
            s = '"';
            s += *m_string;
            s += '"';
            break;
        case JsonType::Integer:
            s = InternalTypeConversion<integer_type, string_type>::Convert(m_integer);
            break;
        case JsonType::Float:
            s = InternalTypeConversion<float_type, string_type>::Convert(m_float);
            break;
        case JsonType::Boolean:
            s = InternalTypeConversion<boolean_type, string_type>::Convert(m_boolean);
            break;
        case JsonType::Array:
            InternalArrayToJsonString(s, fmt, column);
            break;
        case JsonType::Object:
            InternalObjectToJsonString(s, fmt, column);
            break;
        default:
            // Nothing to do.
            break;
        }

        return s;
    }

    // Clear existing type and change to new type, if the type is different.
    // Any existing value will be lost.
    void SetType(JsonType type)
    {
        if (m_type != type && !IsReadOnly())
        {
            Clear();
            InternalSetType(type);
        }
    }

    // Change the type and where possible retain the value.
    // Changing to a JSON object from any other type will lose the value
    // and create an empty JSON object.
    void ChangeType(JsonType type)
    {
        if (type != m_type && !IsReadOnly())
        {
            switch (type)
            {
            case JsonType::Null:
                Clear();
                break;
            case JsonType::String:
                ChangeToString();
                break;
            case JsonType::Integer:
                ChangeToInteger();
                break;
            case JsonType::Float:
                ChangeToFloat();
                break;
            case JsonType::Boolean:
                ChangeToBoolean();
                break;
            case JsonType::Array:
                ChangeToArray();
                break;
            case JsonType::Object:
                ThrowException("Use ChangeToObjectType instead of ChangeType");
                break;
            case JsonType::Error:
                CheckNotReadOnly();
                break;
            default:
                // Nothing to do.
                break;
            }
        }
    }

    // Change the type to a JSON object and move the existing value to the key and value pair.
    void ChangeToObjectType(string_type const& key)
    {
        if (!IsObject() && !IsReadOnly())
        {
            json_value_type* value = new json_value_type(std::move(*this));
            InternalSetType(JsonType::Object);
            m_map->insert(object_pair_type(key, value));
        }
    }

    // Return a copy of this object.
    json_value_type* Copy() const
    {
        json_value_type* value = new json_value_type(*this);
        return value;
    }

    // Copy JSON value to this object.
    void Copy(json_value_type const& value)
    {
        if (!IsReadOnly())
        {
            Clear();
            InternalCopy(value);
        }
    }

    // Copy JSON value to this object.
    void Copy(array_type const& value)
    {
        if (!IsReadOnly())
        {
            Clear();
            InternalCopy(value);
        }
    }

    // Copy JSON value to this object.
    void Copy(object_type const& value)
    {
        if (!IsReadOnly())
        {
            Clear();
            InternalCopy(value);
        }
    }

    // Move JSON value to this object.
    void Move(json_value_type& value) noexcept
    {
        if (!IsReadOnly())
        {
            Clear();
            InternalMove(value);
        }
    }

    // Move the value and ensure the value is set to null, ready for re-use.
    void SafeMove(json_value_type& value) noexcept
    {
        if (!IsReadOnly())
        {
            Clear();
            InternalMove(value);
        }
    }

    // Set the value to a None JsonType and free any resources.
    void Clear() noexcept
    {
        switch (m_type)
        {
        case JsonType::Error:
            // Cannot clear the error type.
            break;
        case JsonType::Null:
            // Nothing to clear.
            break;
        case JsonType::String:
            delete m_string;
            m_type = JsonType::Null;
            break;
        case JsonType::Array:
            for (typename array_type::size_type index = 0; index < m_array->size(); ++index)
                delete (*m_array)[index];
            m_array->clear();
            delete m_array;
            m_type = JsonType::Null;
            break;
        case JsonType::Object:
            for (auto iter = m_map->begin(); iter != m_map->end(); ++iter)
                delete iter->second;
            m_map->clear();
            delete m_map;
            m_type = JsonType::Null;
            break;
        case JsonType::Integer:
            [[fallthrough]];
        case JsonType::Float:
            [[fallthrough]];
        default:
            m_type = JsonType::Null;
            break;
        }
    }

    // Compare two JSON values and return true when they match.
    bool Compare(json_value_type const& rhs) const noexcept
    {
        JsonType type = Type();

        if (type == rhs.Type())
        {
            switch (type)
            {
            case JsonType::Null:
                return true;
            case JsonType::Error:
                return true;
            case JsonType::Integer:
                return m_integer == rhs.m_integer;
            case JsonType::Float:
                return m_float == rhs.m_float;
            case JsonType::Boolean:
                return m_boolean == rhs.m_boolean;
            case JsonType::Array:
                if (m_array->size() != rhs.m_array->size())
                    return false;
                for (std::size_t index = 0; index < m_array->size(); ++index)
                    if (!(*m_array)[index]->Compare(*(*rhs.m_array)[index]))
                        return false;
                return true;
            case JsonType::Object:
                if (m_map->size() != rhs.m_map->size())
                    return false;
                for (auto iter = m_map->begin(); iter != m_map->end(); ++iter)
                {
                    auto rhs_iter = rhs.m_map->find(iter->first);
                    if (rhs_iter == rhs.m_map->end())
                        return false;
                    if (!iter->second->Compare(*rhs_iter->second))
                        return false;
                }
                return true;
            default:
                // Unknown type can never match.
                return false;
            }
        }

        return false;
    }

    // Find the first JSON object with a matching key and return the the match or Error().
    json_value_type& FindObject(string_type const& key) noexcept
    {
        json_value_type* value = InternalFindObject(key);
        return value ? *value : Error();
    }

    // Find the first JSON object with a matching key.
    json_value_type const& FindObject(string_type const& key) const noexcept
    {
        return const_cast<json_value_type&>(*this).FindObject(key);
    }

    // Parse the JSON string and return true on success.
    bool Parse(string_ptr_type json_string)
    {
        if (!json_string)
            ThrowException("Parse must have a valid string");

        if (IsReadOnly())
            return false;

        Clear();
        string_ptr_type ret = InternalParse(json_string);
        return ret && *ret == '\0';
    }

    // Parse the JSON string and return true on success.
    bool Parse(std::string const& json_string)
    {
        return Parse(json_string.c_str());
    }

    // When a type doesn't match the null value is returned.
    // E.g. trying to use a JSON value as an array when it's another type will return the null value.
    static json_value_type& Error() noexcept
    {
        static json_value_type error_value(JsonType::Error);
        return error_value;
    }

private:

    //
    // Internal types.
    //
    typedef InternalStringUtility<char_type> string_utility;


    //
    // Internal helper functions.
    //

    // Copy the source JSON array to a destination JSON array without clearing the source value.
    static void InternalCopy(array_type& dst, array_type const& src)
    {
        dst.resize(src.size());
        for (typename array_type::size_type index = 0; index < src.size(); ++index)
            dst[index] = src[index]->Copy();
    }

    // Copy the source JSON object to a destination JSON object without clearing the source value.
    static void InternalCopy(object_type& dst, object_type const& src)
    {
        for (auto iter = src.begin(); iter != src.end(); ++iter)
            dst[iter->first] = iter->second->Copy();
    }

    // Copy the source JSON value to a destination JSON value without clearing the source value.
    static void InternalCopy(json_value_type& dst, json_value_type const& src)
    {
        dst.InternalCopy(src);
    }

    // Move the source JSON array to a destination JSON array without clearing the source value.
    static void InternalMove(array_type& dst, array_type& src)
    {
        dst = std::move(src);
    }

    // Move the source JSON object to a destination JSON object without clearing the source value.
    static void InternalMove(object_type& dst, object_type& src)
    {
        dst = std::move(src);
    }

    // Move the source JSON value to a destination JSON value without clearing the source value.
    static void InternalMove(json_value_type& dst, json_value_type& src)
    {
        dst.InternalMove(src);
    }

    // Move the source JSON array to a destination JSON array without clearing the source value.
    static void InternalSafeMove(array_type& dst, array_type& src)
    {
        dst = std::move(src);
    }

    // Move the source JSON object to a destination JSON object without clearing the source value.
    static void InternalSafeMove(object_type& dst, object_type& src)
    {
        dst = std::move(src);
    }

    // Move the source JSON value to a destination JSON value without clearing the source value.
    static void InternalSafeMove(json_value_type& dst, json_value_type& src)
    {
        dst.InternalMove(src);
    }

    // Set the new data type and initialise to a default value,
    // which assumes the JSON value is already cleared (required for constructor).
    void InternalSetType(JsonType type)
    {
        switch (type)
        {
        case JsonType::String:
            m_type = JsonType::String;
            m_string = new string_type;
            break;
        case JsonType::Array:
            m_type = JsonType::Array;
            m_array = new array_type;
            break;
        case JsonType::Object:
            m_type = JsonType::Object;
            m_map = new object_type;
            break;
        case JsonType::Boolean:
            m_type = JsonType::Boolean;
            m_boolean = false;
            break;
        case JsonType::Integer:
            JsonType::Integer;
            m_integer = 0;
            break;
        case JsonType::Float:
            JsonType::Float;
            m_float = 0.0;
            break;
        case JsonType::Null:
            m_type = JsonType::Null;
            m_string = nullptr;
            break;
        case JsonType::Error:
            [[fallthrough]];
        default:
            m_type = JsonType::Error;
            m_string = nullptr;
            break;
        }
    }

    // Set the new data type and initialise to a default value,
    // which assumes the JSON value is already cleared (required for constructor).
    void InternalSetData(JsonType type)
    {
        switch (type)
        {
        case JsonType::String:
            m_string = new string_type;
            break;
        case JsonType::Array:
            m_array = new array_type;
            break;
        case JsonType::Object:
            m_map = new object_type;
            break;
        case JsonType::Boolean:
            m_boolean = false;
            break;
        case JsonType::Integer:
            m_integer = 0;
            break;
        case JsonType::Float:
            m_float = 0.0;
            break;
        case JsonType::Null:
            [[fallthrough]];
        case JsonType::Error:
            [[fallthrough]];
        default:
            m_string = nullptr;
            break;
        }
    }

    // Copy the JSON array to this JSON value, without clearing this value.
    void InternalCopy(array_type const& a)
    {
        m_type = JsonType::Array;
        m_array = new array_type;
        InternalCopy(*m_array, a);
    }

    // Copy the JSON array to this JSON value, without clearing this value.
    void InternalCopy(object_type const& obj)
    {
        m_type = JsonType::Object;
        m_map = new object_type();
        InternalCopy(*m_map, obj);
    }

    // Copy value without first clearing this value.
    void InternalCopy(json_value_type const& value)
    {
        switch (value.m_type)
        {
        case JsonType::String:
            m_type = JsonType::String;
            m_string = new string_type(*value.m_string);
            break;
        case JsonType::Integer:
            m_type = JsonType::Integer;
            m_integer = value.m_integer;
            break;
        case JsonType::Float:
            m_type = JsonType::Float;
            m_float = value.m_float;
            break;
        case JsonType::Boolean:
            m_type = JsonType::Boolean;
            m_boolean = value.m_boolean;
            break;
        case JsonType::Array:
            InternalCopy(*value.m_array);
            break;
        case JsonType::Object:
            InternalCopy(*value.m_map);
            break;
        case JsonType::Error:
            // Cannot modify error type.
            break;
        case JsonType::Null:
            [[fallthrough]];
        default:
            break;
        }
    }

    // Move the JSON array to this JSON value, without clearing this value.
    void InternalMove(array_type& a)
    {
        m_type = JsonType::Array;
        m_array = new array_type(a.size());
        for (typename array_type::size_type index = 0; index < a.size(); ++index)
            (*m_array)[index] = a[index];
        a.clear();
    }

    // Move the JSON object to this JSON value, without clearing this value.
    void InternalMove(object_type& obj)
    {
        m_type = JsonType::Object;
        m_map = new object_type();
        for (auto iter = obj.begin(); iter != obj.end(); ++iter)
            (*m_map)[iter->first] = iter->second;
        obj.clear();
    }

    // Find the value for the index within the array,
    // or throw an exception if the type is not an array or out of bounds.
    template<typename IntType>
    json_value_type& InternalIndexAt(IntType index)
    {
        CheckArrayIndex(index);
        return *(*m_array)[static_cast<std::size_t>(index)];
    }

    // Find the value for the index within the array,
    // or throw an exception if the type is not an array or out of bounds.
    template<typename IntType>
    json_value_type const& InternalIndexAt(IntType index) const
    {
        return const_cast<json_value_type&>(*this).InternalIndexAt<IntType>(index);
    }


    // Move value and assume clear is not required before the move.
    void InternalMove(json_value_type& value) noexcept
    {
        switch (value.m_type)
        {
        case JsonType::String:
            m_type = JsonType::String;
            m_string = value.m_string;
            break;
        case JsonType::Integer:
            m_type = JsonType::Integer;
            m_integer = value.m_integer;
            break;
        case JsonType::Float:
            m_type = JsonType::Float;
            m_float = value.m_float;
            break;
        case JsonType::Boolean:
            m_type = JsonType::Boolean;
            m_boolean = value.m_boolean;
            break;
        case JsonType::Array:
            m_type = JsonType::Array;
            m_array = value.m_array;
            break;
        case JsonType::Object:
            m_type = JsonType::Object;
            m_map = value.m_map;
            break;
        case JsonType::Error:
            [[fallthrough]];
        case JsonType::Null:
            [[fallthrough]];
        default:
            break;
        }
    }

    // Move value and assume clear is not required before the move.
    void InternalSafeMove(json_value_type& value) noexcept
    {
        switch (value.m_type)
        {
        case JsonType::String:
            m_type = JsonType::String;
            m_string = value.m_string;
            value.m_type = JsonType::Null;
            break;
        case JsonType::Integer:
            m_type = JsonType::Integer;
            m_integer = value.m_integer;
            value.m_type = JsonType::Null;
            break;
        case JsonType::Float:
            m_type = JsonType::Float;
            m_float = value.m_float;
            value.m_type = JsonType::Null;
            break;
        case JsonType::Boolean:
            m_type = JsonType::Boolean;
            m_boolean = value.m_boolean;
            value.m_type = JsonType::Null;
            break;
        case JsonType::Array:
            m_type = JsonType::Array;
            m_array = value.m_array;
            value.m_type = JsonType::Null;
            break;
        case JsonType::Object:
            m_type = JsonType::Object;
            m_map = value.m_map;
            value.m_type = JsonType::Null;
            break;
        case JsonType::Error:
            [[fallthrough]];
        case JsonType::Null:
            [[fallthrough]];
        default:
            m_type = JsonType::Null;
            break;
        }
    }

    // Load the string containing JSON data.
    // Return null when the whole string is parsed, or return the next part of the string for parsing.
    string_ptr_type InternalParse(string_ptr_type json_string)
    {
        for (char ch = *json_string; ch != '\0'; ch = *json_string)
        {
            switch (ch)
            {
            // Match JSON integer or float value.
            case '0':
                [[fallthrough]];
            case '1':
                [[fallthrough]];
            case '2':
                [[fallthrough]];
            case '3':
                [[fallthrough]];
            case '4':
                [[fallthrough]];
            case '5':
                [[fallthrough]];
            case '6':
                [[fallthrough]];
            case '7':
                [[fallthrough]];
            case '8':
                [[fallthrough]];
            case '9':
                [[fallthrough]];
            case '.':
                json_string = InternalParseNumeric(json_string);
                break;

            case 't': // Match JSON boolean true value.
                json_string = InternalParseBooleanTrue(json_string);
                break;

            case 'f': // Match JSON boolean false value.
                json_string = InternalParseBooleanFalse(json_string);
                break;

            case 'n': // Match JSON null value.
                json_string = InternalParseNull(json_string);
                break;

            case '"': // Match JSON string value.
                json_string = InternalParseString(json_string);
                break;

            case '[': // Match JSON array value.
                json_string = InternalParseArray(json_string);
                break;

            case '{': // Match JSON object value.
                json_string = InternalParseObject(json_string);
                break;

            case '\n':
                [[fallthrough]];
            case '\r':
                [[fallthrough]];
            case '\t':
                [[fallthrough]];
            case ' ':
                // Skip any white space that is not within a JSON string.
                ++json_string;
                break;

            case '\0':
                [[fallthrough]];
            default:
                return json_string; // End of current value or buffer.
            }
        }

        return json_string; // End of current value or buffer.
    }

    string_ptr_type InternalParseNumeric(string_ptr_type json_string)
    {
        if (m_type != JsonType::Null)
            ThrowNotNullException(json_string);

        // When an integer or float is discovered, parse the whole sequence.
        bool is_integer = false;
        string_ptr_type next = InternalJsonStringParser<integer_type>::Extract(json_string, m_integer, m_float, is_integer);

        if (!next)
            ThrowException("Failed to extract integer or float", json_string);

        m_type = is_integer ? JsonType::Integer : JsonType::Float;
        return next;
    }

    string_ptr_type InternalParseBooleanTrue(string_ptr_type json_string)
    {
        if (m_type != JsonType::Null)
            ThrowNotNullException(json_string);

        if (!string_utility::Compare(json_string, "true", 4))
            ThrowException("Unknown boolean value, expecting true", json_string);

        m_type = JsonType::Boolean;
        m_boolean = true;
        return json_string + 4;
    }

    string_ptr_type InternalParseBooleanFalse(string_ptr_type json_string)
    {
        if (m_type != JsonType::Null)
            ThrowNotNullException(json_string);

        if (!string_utility::Compare(json_string, "false", 5))
            ThrowException("Unknown boolean value, expecting false", json_string);

        m_type = JsonType::Boolean;
        m_boolean = false;
        return json_string + 5;
    }

    string_ptr_type InternalParseNull(string_ptr_type json_string)
    {
        if (m_type != JsonType::Null)
            ThrowNotNullException(json_string);

        if (!string_utility::Compare(json_string, "null", 4))
            ThrowException("Unknown value, expecting null", json_string);

        return json_string + 4;
    }

    string_ptr_type InternalParseString(string_ptr_type json_string)
    {
        // When a string is discovered, parse the whole sequence.
        if (m_type != JsonType::Null)
            ThrowNotNullException(json_string);

        InternalSetType(JsonType::String);
        string_ptr_type next = InternalJsonStringParser<string_type>::Extract(json_string, *m_string);

        if (!next)
            ThrowException("Failed to parse string value", json_string);

        return next;
    }

    // Load the array, which may recursively load other arrays or objects.
    string_ptr_type InternalParseArray(string_ptr_type json_string)
    {
        if (m_type != JsonType::Null)
            ThrowNotNullException(json_string);

        InternalSetType(JsonType::Array);
        string_ptr_type next = string_utility::SkipWhiteSpace(json_string + 1);

        if (*next == '\0')
            ThrowException("Unexpected end of array, expecting ]", json_string);

        json_string = next;

        while (*json_string != ']')
        {
            // Load the next JSON value and add to the JSON array.
            json_value_type* value = new json_value_type;
            next = value->InternalParse(json_string);
            if (next > json_string)
            {
                json_string = next;
                m_array->push_back(value);
                json_string = string_utility::FindNextValue(json_string);
            }
            else
            {
                delete value; // Prevent a failure of not moving through the string buffer.
                ThrowException("Cannot parse array beyond this point", json_string);
            }
        }

        return json_string + 1;
    }

    // Load the object, which may recursively load other arrays or objects.
    string_ptr_type InternalParseObject(string_ptr_type json_string)
    {
        if (m_type != JsonType::Null)
            ThrowNotNullException(json_string);

        InternalSetType(JsonType::Object);

        string_ptr_type key;
        std::size_t count;
        json_string = string_utility::SkipWhiteSpace(json_string + 1);

        // Load JSON key and value pairs and add to the JSON object.
        char ch = *json_string;
        while (ch != '\0')
        {
            if (ch == '}')
            {
                ++json_string; // Move past '}'
                break;
            }

            count = 0;
            string_ptr_type next = InternalJsonStringParser<string_type>::Extract(json_string, key, count);
            if (count > 0)
            {
                json_string = next;
                next = string_utility::SkipWhiteSpace(json_string);
                if (*next == ':')
                {
                    json_value_type* value = new json_value_type;
                    json_string = next + 1;
                    next = value->InternalParse(json_string);
                    if (next)
                    {
                        m_map->emplace(object_pair_type(string_type(key, count), value));
                        json_string = string_utility::FindNextValue(next); // Skip white space and comma.
                        ch = *json_string;
                    }
                    else
                    {
                        delete value;
                        ThrowException("Expecting a valid JSON value for the JSON object", json_string);
                    }
                }
                else
                    ThrowException("Expecting \':' for JSON object", json_string);
            }
            else
                ThrowException("Cannot parse JSON object for key value", json_string);
        }

        return json_string;
    }

    // Convert the value to a JSON string.
    // JSON string is returned between " ",
    // JSON boolean is returned as true or false,
    // JSON array is returned between [ ] and , separating values,
    // JSON object is returned between { } with : used to separate key and value,
    // and , separating the key and value pairs.
    void InternalToJsonString(string_type& s) const
    {
        switch (m_type)
        {
        case JsonType::Null:
            s += "null";
            break;
        case JsonType::String:
            s + '"';
            s += *m_string;
            s += '"';
            break;
        case JsonType::Integer:
            s += InternalTypeConversion<integer_type, string_type>::Convert(m_integer);
            break;
        case JsonType::Float:
            s += InternalTypeConversion<float_type, string_type>::Convert(m_float);
            break;
        case JsonType::Boolean:
            s += InternalTypeConversion<boolean_type, string_type>::Convert(m_boolean);
            break;
        case JsonType::Array:
            InternalArrayToJsonString(s);
            break;
        case JsonType::Object:
            InternalObjectToJsonString(s);
            break;
        case JsonType::Error:
            ThrowException("Cannot convert error to a JSON string");
            break;
        default:
            break;
        }
        return s;
    }

    // Convert the value to a JSON string with formatting.
    // JSON string is returned between " ",
    // JSON boolean is returned as true or false,
    // JSON array is returned between [ ] and , separating values,
    // JSON object is returned between { } with : used to separate key and value,
    // and , separating the key and value pairs.
    void InternalToJsonString(string_type& s, json_format_type const& fmt, std::size_t column) const
    {
        switch (m_type)
        {
        case JsonType::Null:
            s += "null";
            break;
        case JsonType::String:
            s + '"';
            s += *m_string;
            s += '"';
            break;
        case JsonType::Integer:
            s += InternalTypeConversion<integer_type, string_type>::Convert(m_integer);
            break;
        case JsonType::Float:
            s += InternalTypeConversion<float_type, string_type>::Convert(m_float);
            break;
        case JsonType::Boolean:
            s += InternalTypeConversion<boolean_type, string_type>::Convert(m_boolean);
            break;
        case JsonType::Array:
            InternalArrayToJsonString(s, fmt, column);
            break;
        case JsonType::Object:
            InternalObjectToJsonString(s, fmt, column);
            break;
        case JsonType::Error:
            [[fallthrough]];
        default:
            break;
        }
        return s;
    }

    void InternalArrayToJsonString(string_type& s) const
    {
        s += '[';
        for (typename array_type::size_type index = 0; index < m_array->size(); ++index)
        {
            json_value_type* value = (*m_array)[index];
            if (index > 0)
                s += ',';
            value->InternalToJsonString(s);
        }
        s += ']';
    }

    void InternalArrayToJsonString(string_type& s, json_format_type const& fmt, std::size_t column) const
    {
        string_type indent = fmt.IndentString(column);
        if (fmt.ArrayNewLineBefore())
        {
            s += fmt.eol;
            s += indent;
        }
        s += '[';
        if (fmt.ArrayNewLineAfter())
        {
            if (fmt.width < 0)
                s += fmt.IndentString(column + 1);
            s += fmt.eol;
        }
        for (typename array_type::size_type index = 0; index < m_array->size(); ++index)
        {
            json_value_type* value = (*m_array)[index];
            if (index > 0)
                s += ',';
            value->InternalToJsonString(s, fmt, column + 1);
        }
        if (fmt.ArrayNewLineBefore())
        {
            s += fmt.eol;
            s += indent;
        }
        s += ']';
        if (fmt.ArrayNewLineAfter())
            s += fmt.eol;
    }

    void InternalObjectToJsonString(string_type& s) const
    {
        s += '{';
        for (auto iter = m_map->begin(); iter != m_map->end(); ++iter)
        {
            json_value_type* value = iter->second;
            if (iter != m_map->begin())
                s += ',';
            s += '"';
            s += *m_string;
            s += '"';
            s += ':';
            value->InternalToJsonString(s);
        }
        s += '}';
    }

    void InternalObjectToJsonString(string_type& s, json_format_type const& fmt, std::size_t column) const
    {
        string_type indent = fmt.IndentString(column);
        if (fmt.ObjectNewLineBefore())
        {
            s += fmt.eol;
            s += indent;
        }
        s += '{';
        if (fmt.ObjectNewLineAfter())
        {
            if (fmt.width < 0)
                s += fmt.IndentString(column + 1);
            s += fmt.eol;
        }
        for (auto iter = m_map->begin(); iter != m_map->end(); ++iter)
        {
            json_value_type* value = iter->second;
            if (iter != m_map->begin())
                s += ',';
            s += '"';
            s += *m_string;
            s += '"';
            s += ':';
            value->InternalToJsonString(s);
        }
        if (fmt.ObjectNewLineBefore())
        {
            s += fmt.eol;
            s += indent;
        }
        s += '}';
        if (fmt.ObjectNewLineAfter())
            s += fmt.eol;
    }

    // Find the first JSON object with a matching key and return the value or null if not found.
    json_value_type* InternalFindObject(string_type const& key) noexcept
    {
        json_value_type* value = nullptr;

        if (IsObject())
        {
            auto iter = m_map->find(key);
            if (iter != m_map->end())
                value = iter->second;
            else
            {
                // Check all JSON object values for other objects or arrays and match the key.
                for (iter = m_map->begin(); iter != m_map->end(); ++iter)
                {
                    value = iter->second;
                    if (value->IsObject() || value->IsArray())
                    {
                        value = value->InternalFindObject(key);
                        if (!value)
                            break;
                    }
                }
            }
        }
        else if (IsArray())
        {
            // Check all array items for a JSON object with a matching key.
            for (typename array_type::size_type index = 0; index < m_array->size(); ++index)
            {
                value = (*m_array)[index];
                if (value->IsObject() || value->IsArray())
                {
                    value = value->InternalFindObject(key);
                    if (!value)
                        break;
                }
            }
        }

        return value;
    }


    //
    // Member data.
    //

    JsonType m_type; // JSON type of string, integer, float, boolean, list, object, or null.

    union
    {
        string_type* m_string;  // JsonType::String
        integer_type m_integer;
        float_type   m_float;
        boolean_type m_boolean;
        array_type*  m_array;
        object_type* m_map;
    };
};

// json namespace provides the default JsonDocument using types
// std::string, long long, double and bool for JSON string, integer, float and boolean.
namespace json
{

typedef JsonValue<> Value;

typedef typename Value::JsonType JsonType;

// Character type supported for JSON strings.
typedef typename Value::char_type CharType;

// Return type for string::c_str.
typedef typename Value::string_ptr_type JsoStringPtrType;

// JSON types that are not an array or object.
typedef typename Value::string_type  StringType;
typedef typename Value::integer_type IntegerType;
typedef typename Value::float_type   FloatType;
typedef typename Value::boolean_type BooleanType;

} // namespace json

} // namespace ocl

#endif // OCL_GUARD_JSONVALUE_HPP
