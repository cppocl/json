# json

C++ header only JSON parser, which can parse a string or file, and save with formatting.

# Status

This is currently under development and not yet fully tested.

# Alternatives

There are other C++ JSON parsers that can be considered, as this is currently a personal project to experiment with JSON features.

[https://github.com/nlohmann/json](https://github.com/nlohmann/json)

[https://github.com/Tencent/rapidjson/](https://github.com/Tencent/rapidjson/)

# Examples
```
#include "json/JsonParser.hpp"
#include <iostream>

ocl::json::Parser parser;
if (parser.Load("json/unit_tests/test_files/json_complete_test.json"))
{
    typedef ocl::json::Value const& value = parser.Value();
    std::cout << value["int_value"].GetInteger() << std::endl;
    std::cout << value["object"]["d"].GetString() << std::endl;
}
```
